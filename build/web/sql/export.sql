--------------------------------------------------------
--  File created - Tuesday-September-24-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table USUARIOS
--------------------------------------------------------

  CREATE TABLE "TEST"."USUARIOS" 
   (	"CODUSU" CHAR(4 BYTE), 
	"NOMUSU" VARCHAR2(30 BYTE), 
	"EDADUSU" NUMBER(*,0), 
	"SEXOUSU" CHAR(1 BYTE), 
	"PASSUSU" VARCHAR2(10 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Sequence SEC_USUARIO_ID
--------------------------------------------------------

REM INSERTING into TEST.USUARIOS
SET DEFINE OFF;
Insert into TEST.USUARIOS (CODUSU,NOMUSU,EDADUSU,SEXOUSU,PASSUSU) values ('U001','Jose',33,'M','1111');
Insert into TEST.USUARIOS (CODUSU,NOMUSU,EDADUSU,SEXOUSU,PASSUSU) values ('U002','Maria',35,'F','2222');
Insert into TEST.USUARIOS (CODUSU,NOMUSU,EDADUSU,SEXOUSU,PASSUSU) values ('U003','Ana',30,'F','3333');
Insert into TEST.USUARIOS (CODUSU,NOMUSU,EDADUSU,SEXOUSU,PASSUSU) values ('U004','Ruben',29,'M','4444');
Insert into TEST.USUARIOS (CODUSU,NOMUSU,EDADUSU,SEXOUSU,PASSUSU) values ('U005','Dario',48,'M','5555');
--------------------------------------------------------
--  Constraints for Table USUARIOS
--------------------------------------------------------

  ALTER TABLE "TEST"."USUARIOS" MODIFY ("PASSUSU" NOT NULL ENABLE);
  ALTER TABLE "TEST"."USUARIOS" MODIFY ("SEXOUSU" NOT NULL ENABLE);
  ALTER TABLE "TEST"."USUARIOS" MODIFY ("EDADUSU" NOT NULL ENABLE);
  ALTER TABLE "TEST"."USUARIOS" MODIFY ("NOMUSU" NOT NULL ENABLE);
  ALTER TABLE "TEST"."USUARIOS" MODIFY ("CODUSU" NOT NULL ENABLE);
